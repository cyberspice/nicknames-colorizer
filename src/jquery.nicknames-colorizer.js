/**
 *    Nicknames colorizer.
 *    jQuery plugin for colorizing the nicknames of mIRC logs.
 *    Created by Alexey Norkin (cyberSpice), 2018.
 */
(function ($) {
    $.fn.colorfy = function (nicknameClassPattern) {
        return this.each(function () {
            let content = $(this).html();
            let rawNicknames = content.match(/&lt;.*?&gt;/g);
            if (rawNicknames != null) {
                rawNicknames = jQuery.uniqueSort(rawNicknames);
                for (let i = 0; i < rawNicknames.length; i++) {
                    let nickname = rawNicknames[i].replace(/&lt;/g, '').replace(/&gt;/g, '');
                    let re = new RegExp(nickname, "g");
                    content = content.replace(re, "<span class='" + nicknameClassPattern + (i + 1) + "'>" + nickname + "</span>");
                }
                $(this).html(content);
            }
        });
    };

}(jQuery));